package submod

import "fmt"

func SayHello() {
	fmt.Println("Hello from hellomod module (v1.0.2)")
}
